import 'package:clima/core/errors/exceptions.dart';
import 'package:clima/core/errors/failure.dart';
import 'package:clima/core/geolocator/geo_locator.dart';
import 'package:clima/core/networkinfo/network_info.dart';
import 'package:clima/features/weather/data/datasources/weather_remote_data_source.dart';
import 'package:clima/features/weather/domain/entities/weather.dart';
import 'package:clima/features/weather/domain/repositories/weather_repository.dart';
import 'package:dartz/dartz.dart';

import '../models/weather_model.dart';

class WeatherRepositoryImpl implements WeatherRepository {
  final WeahterRemoteDataSource weahterRemoteDataSource;
  final GeoLocatorService locatorService;
  final NetworkInfo networkInfo;
  WeatherRepositoryImpl(
      this.weahterRemoteDataSource, this.locatorService, this.networkInfo);

  @override
  Future<Either<Failure, Weather>> getWeatherFromLocation() async {
    try {
      final position = await locatorService.getPosition();
      return await _getWeather(() async {
        return await weahterRemoteDataSource.getWeatherFromLocation(
            position.longitude, position.latitude);
      });
    } on GeoException catch (exception) {
      return Left(GeoFailure(exception.message));
    } 
  }

  @override
  Future<Either<Failure, Weather>> searchWeather(String cityName) async {
    return await _getWeather(() async {
      return await weahterRemoteDataSource.searchWeather(cityName);
    });
  }

  Future<Either<Failure, Weather>> _getWeather(
      Future<WeatherModel> Function() positionOrSearch) async {
    if (await networkInfo.isConnected) {
      try {
        final weather = await positionOrSearch();
        return Right(weather);
      } on ServerException catch (_) {
        return Left(ServerFailure());
      }
    } else {
      return Left(NetworkFailure());
    }
  }
}
