// ignore_for_file: constant_identifier_names

import 'dart:convert';

import 'package:clima/core/errors/exceptions.dart';
import 'package:clima/features/weather/data/models/weather_model.dart';
import 'package:http/http.dart' as http;

const APP_ID = '5c5fb4aeb2e5c8859204c917f966c636';

abstract class WeahterRemoteDataSource {
  /// Should call https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={appid}
  ///
  /// will through [ServerException] if there was an erro
  Future<WeatherModel> getWeatherFromLocation(double long, double lat);

  /// Should call https://api.openweathermap.org/data/2.5/weather?q=Hama&appid={appid}
  ///
  /// will through [ServerException] if there was an erro
  Future<WeatherModel> searchWeather(String cityName);
}

class WeatherRemoteDataSourceImpl implements WeahterRemoteDataSource {
  final http.Client client;

  WeatherRemoteDataSourceImpl(this.client);
  @override
  Future<WeatherModel> getWeatherFromLocation(double long, double lat) =>
      _getWeatherFromUrl(
        'https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$long&appid=$APP_ID&units=metric',
      );

  @override
  Future<WeatherModel> searchWeather(String cityName) => _getWeatherFromUrl(
        'https://api.openweathermap.org/data/2.5/weather?q=$cityName&appid=$APP_ID&units=metric',
      );

  Future<WeatherModel> _getWeatherFromUrl(String uri) async {
    final response = await client.get(Uri.parse(uri));
    if (response.statusCode == 200) {
      return WeatherModel.fromJson(jsonDecode(response.body));
    } else {
      throw ServerException();
    }
  }
}
