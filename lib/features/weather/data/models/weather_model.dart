import 'package:clima/features/weather/domain/entities/weather.dart';

class WeatherModel extends Weather {
  const WeatherModel(
      {required super.cityName,
      required super.description,
      required super.icon,
      required super.humidity,
      required super.sunRise,
      required super.sunSet,
      required super.temp,
      required super.windSpeed,
      required super.id, required super.timeZone});

  factory WeatherModel.fromJson(Map<String, dynamic> json) {
    return WeatherModel(
      id: json['weather'][0]['id'],
      cityName: json['name'],
      description: json['weather'][0]['description'],
      icon: json['weather'][0]['icon'],
      humidity: json['main']['humidity'],
      sunRise:
          DateTime.fromMillisecondsSinceEpoch(json['sys']['sunrise'] * 1000),
      sunSet: DateTime.fromMillisecondsSinceEpoch(json['sys']['sunset'] * 1000),
      temp: json['main']['temp'],
      windSpeed: json['wind']['speed'],
      timeZone: json['timezone']
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'name': cityName,
      'weather': [
        {'description': description, 'icon': icon, 'id': id}
      ],
      'main': {'humidity': humidity, 'temp': temp},
      'sys': {
        'sunrise': sunRise.millisecondsSinceEpoch ~/ 1000,
        'sunset': sunSet.millisecondsSinceEpoch ~/ 1000
      },
      'wind': {'speed': windSpeed},
      'timezone':timeZone
    };
  }
}
