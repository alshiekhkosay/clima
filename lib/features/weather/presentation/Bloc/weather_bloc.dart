// ignore_for_file: constant_identifier_names

import 'package:bloc/bloc.dart';
import 'package:clima/core/errors/failure.dart';
import 'package:clima/features/weather/domain/entities/weather.dart';
import 'package:clima/features/weather/domain/usecases/get_weather_from_location.dart';
import 'package:clima/features/weather/domain/usecases/search_weather.dart';
import 'package:equatable/equatable.dart';

part 'weather_event.dart';
part 'weather_state.dart';

const NETWORK_FAILURE_MESSAGE = 'There is no internet connection';
const SERVER_FAILURE = 'Server Failure';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  final GetWeatherFromLocation getWeatherFromLocation;
  final SearchWeather searchWeather;
  WeatherBloc({
    required this.getWeatherFromLocation,
    required this.searchWeather,
  }) : super(WeatherInitial()) {
    on<WeatherEvent>((event, emit) async {
      if (event is GetWeatherForLocation) {
        emit(Loading());
        final weatherOrFailure = await getWeatherFromLocation(const NoParams());
        await weatherOrFailure.fold((failure) async {
          _mapFailureToError(failure, emit);
        }, (weather) async {
          emit(Loaded(weather));
        });
      } else if (event is SearchWeatherWithCityName) {
        emit(SearchLoading());
        final weatherOrFailure =
            await searchWeather(SearchParams(event.cityName));
        await weatherOrFailure.fold((failure) async {
          if (failure is GeoFailure) {
            emit(SearchError(failure.message));
          } else if (failure is NetworkFailure) {
            emit(const SearchError(NETWORK_FAILURE_MESSAGE));
          } else if (failure is ServerFailure) {
            emit(const SearchError(SERVER_FAILURE));
          }
        }, (weather) async {
          emit(SearchLoaded(weather));
        });
      }
    });
  }

  void _mapFailureToError(Failure failure, Emitter<WeatherState> emit) {
    if (failure is GeoFailure) {
      emit(Error(failure.message));
    } else if (failure is NetworkFailure) {
      emit(const Error(NETWORK_FAILURE_MESSAGE));
    } else if (failure is ServerFailure) {
      emit(const Error(SERVER_FAILURE));
    }
  }
}
