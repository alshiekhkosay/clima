part of 'weather_bloc.dart';

sealed class WeatherEvent extends Equatable {
  const WeatherEvent();

  @override
  List<Object> get props => [];
}

class GetWeatherForLocation extends WeatherEvent {}

class SearchWeatherWithCityName extends WeatherEvent {
  final String cityName;

  const SearchWeatherWithCityName(this.cityName);

  @override
  List<Object> get props => [cityName];
}
