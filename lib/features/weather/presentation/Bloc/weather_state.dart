part of 'weather_bloc.dart';

sealed class WeatherState extends Equatable {
  const WeatherState();

  @override
  List<Object> get props => [];
}

final class WeatherInitial extends WeatherState {}

final class SearchInitial extends WeatherState {}

final class SearchLoading extends WeatherState {}

final class Loading extends WeatherState {}

final class SearchLoaded extends WeatherState {
  final Weather weather;

  const SearchLoaded(this.weather);

  @override
  List<Object> get props => [weather];
}

final class Loaded extends WeatherState {
  final Weather weather;

  const Loaded(this.weather);

  @override
  List<Object> get props => [weather];
}

final class SearchError extends WeatherState {
  final String message;

  const SearchError(this.message);
  
  @override
  List<Object> get props => [message];
}

final class Error extends WeatherState {
  final String message;

  const Error(this.message);

  @override
  List<Object> get props => [message];
}
