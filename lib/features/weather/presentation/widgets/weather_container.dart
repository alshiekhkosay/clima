import 'dart:async';

import 'package:clima/core/app_const.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../domain/entities/weather.dart';

class WeatherContainer extends StatefulWidget {
  const WeatherContainer({super.key, required this.weather});
  final Weather weather;

  @override
  State<WeatherContainer> createState() => _WeatherContainerState();
}

class _WeatherContainerState extends State<WeatherContainer> {
  late DateTime dateTime;
  late Timer timer;
  @override
  void initState() {
    dateTime =
        DateTime.now().toUtc().add(Duration(seconds: widget.weather.timeZone));
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        dateTime = DateTime.now()
            .toUtc()
            .add(Duration(seconds: widget.weather.timeZone));
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 180,
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 8.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Theme.of(context).shadowColor.withOpacity(0.5),
              blurRadius: 12,
              offset: const Offset(0, 6)),
        ],
        image: DecorationImage(
          image: AppConsts.chooseImage(widget.weather.id),
          fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(
              Colors.white.withOpacity(0.65), BlendMode.dstATop),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.weather.cityName,
            style: const TextStyle(
                color: Colors.black, fontSize: 18, fontWeight: FontWeight.w500),
          ),
          Text(
            DateFormat.E().add_M().add_MMM().add_jm().format(dateTime),
            style: const TextStyle(color: Colors.black, fontSize: 13),
          ),
          const Spacer(),
          Row(
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              const SizedBox(
                width: 10,
              ),
              Image.network(
                'https://openweathermap.org/img/w/${widget.weather.icon}.png',
                height: 50,
                width: 50,
              ),
              const SizedBox(
                width: 15,
              ),
              Text(
                '${widget.weather.temp.toStringAsFixed(0)}°',
                style: const TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.w400,
                  color: Colors.black,
                ),
              ),
            ],
          ),
          Row(
            children: [
              const SizedBox(
                width: 10,
              ),
              Text(
                widget.weather.description,
                style: const TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.w600),
              ),
            ],
          )
        ],
      ),
    );
  }
}
