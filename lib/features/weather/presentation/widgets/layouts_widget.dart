import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:weather_icons/weather_icons.dart';

import '../../domain/entities/weather.dart';
import '../general/layout_widget.dart';

List<Widget> layoutsWidget(Weather weather) => [
      LayoutWidget(
        title: 'Humidity',
        value: '${weather.humidity}%',
        icon: WeatherIcons.humidity,
        id: weather.id,
      ),
      LayoutWidget(
        title: 'Wind',
        value: '${weather.windSpeed}KM',
        icon: WeatherIcons.cloudy_windy,
        id: weather.id,
      ),
      LayoutWidget(
        title: 'Sunrise',
        value: DateFormat.jm().format(weather.sunRise),
        icon: WeatherIcons.sunrise,
        id: weather.id,
      ),
      LayoutWidget(
        title: 'Sunset',
        value: DateFormat.jm().format(weather.sunSet),
        icon: WeatherIcons.sunset,
        id: weather.id,
      ),
    ];
