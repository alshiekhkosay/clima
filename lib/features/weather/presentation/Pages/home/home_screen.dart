import 'dart:async';

import 'package:clima/features/weather/presentation/Pages/search/search_page.dart';
import 'package:clima/features/weather/presentation/widgets/weather_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../domain/entities/weather.dart';
import '../../Bloc/weather_bloc.dart';
import '../../widgets/layouts_widget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late Weather weather;
  final _key = GlobalKey<RefreshIndicatorState>();
  Completer _refreshCompleter = Completer();
  bool _searching = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0XFFE6E6E6),
      appBar: AppBar(
        backgroundColor: const Color(0XFFE6E6E6),
        elevation: 0,
        title: _buildTitile(context),
        actions: [_buildActionButton()],
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
          child: BlocConsumer<WeatherBloc, WeatherState>(
            listener: _weatherBlocListener,
            builder: (context, state) {
              if (state is Loaded) {
                weather = state.weather;
              }

              return RefreshIndicator(
                onRefresh: () async {
                  _refreshCompleter = Completer();
                  BlocProvider.of<WeatherBloc>(context)
                      .add(GetWeatherForLocation());
                  return _refreshCompleter.future;
                },
                key: _key,
                child: SingleChildScrollView(
                  physics: const AlwaysScrollableScrollPhysics(),
                  child: Column(children: <Widget>[
                    WeatherContainer(weather: weather),
                    const SizedBox(height: 20),
                    ...layoutsWidget(weather)
                  ]),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _buildTitile(BuildContext context) {
    if (_searching) {
      return TextField(
        onSubmitted: (value) {
          BlocProvider.of<WeatherBloc>(context)
              .add(SearchWeatherWithCityName(value));
        },
        decoration: const InputDecoration(
          hintText: 'Search with city name!',
          border: InputBorder.none,
        ),
      );
    }
    return Text('CLIMA', style: Theme.of(context).textTheme.titleLarge);
  }

  Widget _buildActionButton() {
    return Padding(
      padding: const EdgeInsets.only(right: 10.0),
      child: GestureDetector(
        onTap: () => setState(() {
          _searching = !_searching;
        }),
        child: _searching
            ? const Icon(Icons.close, color: Colors.blue)
            : Transform.flip(
                flipX: true,
                child: const Icon(Icons.search, color: Colors.blue),
              ),
      ),
    );
  }

  void _weatherBlocListener(context, state) {
    switch (state.runtimeType) {
      case Loaded:
        _refreshCompleter.complete();
        break;
      case SearchLoading:
        _showLoading(context);
      case SearchLoaded:
        _searchComplete(context, state);
        break;
      case SearchError:
        Navigator.pop(context);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(state.message),
          backgroundColor: Colors.red,
        ));
        break;
    }
  }

  void _searchComplete(context, state) {
    setState(() {
      _searching = false;
    });
    Navigator.pop(context);
    Navigator.push(context,
        MaterialPageRoute(builder: (_) => SearchPage(weather: state.weather)));
  }

  void _showLoading(context) {
    showDialog(
      context: context,
      builder: (_) => const AlertDialog(
        content: Row(
          children: [
            CircularProgressIndicator(),
            SizedBox(width: 10),
            Text('Loading')
          ],
        ),
      ),
    );
  }
}
