import 'dart:developer';

import 'package:clima/features/weather/presentation/Bloc/weather_bloc.dart';
import 'package:clima/features/weather/presentation/Pages/home/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation _animation;

  @override
  void initState() {
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 1750));

    _animation = ColorTween(begin: Colors.blue, end: Colors.orange)
        .animate(_animationController)
      ..addListener(() => setState(() {}))
      ..addStatusListener((status) {
        if (_animationController.status == AnimationStatus.completed) {
          _animationController.reverse();
        } else if (_animationController.status == AnimationStatus.dismissed) {
          _animationController.forward();
        }
      });

    _animationController.forward();
    _initWeather();
    super.initState();
  }

  void _initWeather() {
    BlocProvider.of<WeatherBloc>(context, listen: false)
        .add(GetWeatherForLocation());
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<WeatherBloc, WeatherState>(
      listener: (context, state) async {
        log(state.toString());
        if (state is Error) {
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Text(state.message),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                        _initWeather();
                      },
                      child: const Text('Retry!'))
                ],
              );
            },
          );
        } else if (state is Loaded) {
          final weather = state.weather;
          final networkImage = NetworkImage(
              'https://openweathermap.org/img/w/${weather.icon}.png');
          await precacheImage(networkImage, context).then((value) {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (_) => const HomeScreen(),
                ));
          });
        }
      },
      child: Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              Expanded(
                child: SpinKitFoldingCube(
                  size: 30,
                  color: _animation.value,
                ),
              ),
              const Text(
                'CLIMA',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w900,
                    color: Colors.orange),
              ),
              const Text(
                'Get Your Weather',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14, color: Colors.grey),
              )
            ],
          ),
        ),
      ),
    );
  }
}
