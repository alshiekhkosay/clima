import 'package:flutter/material.dart';

import '../../../domain/entities/weather.dart';
import '../../widgets/layouts_widget.dart';
import '../../widgets/weather_container.dart';

class SearchPage extends StatelessWidget {
  const SearchPage({super.key, required this.weather});
  final Weather weather;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0XFFE6E6E6),
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black, // change the color here
        ),
        backgroundColor: const Color(0XFFE6E6E6),
        elevation: 0,
        title: Text(weather.cityName,
            style: Theme.of(context).textTheme.titleLarge),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(children: <Widget>[
              WeatherContainer(weather: weather),
              const SizedBox(height: 20),
              ...layoutsWidget(weather)
            ]),
          ),
        ),
      ),
    );
  }
}
