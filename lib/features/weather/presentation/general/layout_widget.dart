// ignore_for_file: sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:weather_icons/weather_icons.dart';
import 'package:url_launcher/url_launcher.dart' as url_launcher;

class LayoutWidget extends StatelessWidget {
  const LayoutWidget(
      {Key? key,
      required this.title,
      required this.value,
      required this.icon,
      required this.id})
      : super(key: key);
  final String title;
  final String value;
  final int id;
  final IconData icon;
  @override
  Widget build(BuildContext context) {
    return Container(
        // padding: const EdgeInsets.fromLTRB(10, 15, 10, 15),
        margin: const EdgeInsets.fromLTRB(0, 2, 0, 4),
        height: 60,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: RawMaterialButton(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          onPressed: () async {
            String uri = 'https://openweathermap.org/city/$id';
            if (await url_launcher.canLaunchUrl(Uri.parse(uri))) {
              await url_launcher.launchUrl(Uri.parse(uri));
            }
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Row(
              children: [
                BoxedIcon(icon, size: 20, color: Colors.blueGrey),
                const SizedBox(width: 10),
                Text(
                  '$title :',
                  style: const TextStyle(fontSize: 15, color: Colors.blueGrey),
                ),
                const Spacer(),
                Text(
                  value,
                  textAlign: TextAlign.right,
                  style: const TextStyle(fontSize: 14, color: Colors.blueGrey),
                ),
              ],
            ),
          ),
        ));
  }
}
