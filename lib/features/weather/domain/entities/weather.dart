import 'package:equatable/equatable.dart';

class Weather extends Equatable {
  final int id;
  final String cityName;
  final String description;
  final String icon;
  final int humidity;
  final DateTime sunRise;
  final DateTime sunSet;
  final double temp;
  final double windSpeed;
  final int timeZone;

  const Weather({
    required this.id,
    required this.cityName,
    required this.description,
    required this.icon,
    required this.humidity,
    required this.sunRise,
    required this.sunSet,
    required this.temp,
    required this.windSpeed,
    required this.timeZone,
  });

  @override
  List<Object?> get props =>
      [cityName, temp, humidity, description, sunRise, sunSet, windSpeed];
}
