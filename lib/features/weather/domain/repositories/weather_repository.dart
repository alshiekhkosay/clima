import 'package:dartz/dartz.dart';

import '../../../../core/errors/failure.dart';
import '../entities/weather.dart';

abstract class WeatherRepository {
  Future<Either<Failure, Weather>> getWeatherFromLocation();
  Future<Either<Failure, Weather>> searchWeather(String cityName);
}
