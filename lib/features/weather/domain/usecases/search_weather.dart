import 'package:clima/core/errors/failure.dart';
import 'package:clima/features/weather/domain/repositories/weather_repository.dart';

import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/usecase/usecase.dart';
import '../entities/weather.dart';

class SearchWeather extends UseCase<Weather, SearchParams> {
  final WeatherRepository weatherRepository;

  SearchWeather(this.weatherRepository);
  @override
  Future<Either<Failure, Weather>> call(params) async {
    return await weatherRepository.searchWeather(params.text);
  }
}

class SearchParams extends Equatable {
  final String text;

  const SearchParams(this.text);

  @override
  List<Object?> get props => [text];
}
