import 'package:clima/core/errors/failure.dart';
import 'package:clima/core/usecase/usecase.dart';
import 'package:clima/features/weather/domain/repositories/weather_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../entities/weather.dart';

class GetWeatherFromLocation extends UseCase<Weather, NoParams> {
  final WeatherRepository weatherRepository;

  GetWeatherFromLocation(this.weatherRepository);
  @override
  Future<Either<Failure, Weather>> call(NoParams params) async {
    return await weatherRepository.getWeatherFromLocation();
  }
}

class NoParams extends Equatable {
  

  const NoParams();

  @override
  List<Object?> get props => [];
}
