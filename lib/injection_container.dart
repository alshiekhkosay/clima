import 'package:clima/core/geolocator/geo_locator.dart';
import 'package:clima/core/networkinfo/network_info.dart';
import 'package:clima/features/weather/data/datasources/weather_remote_data_source.dart';
import 'package:clima/features/weather/data/repositories/weather_repository_impl.dart';
import 'package:clima/features/weather/domain/repositories/weather_repository.dart';
import 'package:clima/features/weather/domain/usecases/search_weather.dart';
import 'package:clima/features/weather/presentation/Bloc/weather_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';

import 'features/weather/domain/usecases/get_weather_from_location.dart';

final sl = GetIt.instance;

void init() {
  //weather
  //bloc
  sl.registerFactory(
    () => WeatherBloc(
      getWeatherFromLocation: sl(),
      searchWeather: sl(),
    ),
  );

  //use case
  sl.registerLazySingleton(() => GetWeatherFromLocation(sl()));
  sl.registerLazySingleton(() => SearchWeather(sl()));

  //repository
  sl.registerLazySingleton<WeatherRepository>(
      () => WeatherRepositoryImpl(sl(), sl(), sl()));

  //data source
  sl.registerLazySingleton<WeahterRemoteDataSource>(
      () => WeatherRemoteDataSourceImpl(sl()));
  
  //core
  sl.registerLazySingleton<NetworkInfo>(
      () => NetworkInfoImpl(sl()));
      
  //external
  sl.registerLazySingleton<GeoLocatorService>(() => GeoLocatorServiceImpl());
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton(() => InternetConnectionChecker());
}
