// ignore_for_file: constant_identifier_names

import 'package:flutter/material.dart';

class AppConsts {
  static const List<ImageProvider> IMAGE_ASSETS = [
    AssetImage('assets/images/thunderstorm.jpg'), //0
    AssetImage('assets/images/Drizzle.jpg'), //1
    AssetImage('assets/images/rain.jpg'), //2
    AssetImage('assets/images/freezing_rain.jpg'), //3
    AssetImage('assets/images/snow.jpg'), //4
    AssetImage('assets/images/clearsky.jpg'), //5,
    AssetImage('assets/images/fewclouds.jpg'), //6
    AssetImage('assets/images/scatteredclouds.jpg'), //7
    AssetImage('assets/images/overcastclouds.jpg') //8
  ];

  static ImageProvider chooseImage(int id) {
    switch (id) {
      case < 300:
        return IMAGE_ASSETS[0];
      case < 400 || (>= 700 && < 800):
        return IMAGE_ASSETS[1];
      case < 600:
        if (id == 511) return IMAGE_ASSETS[3];
        return IMAGE_ASSETS[2];
      case < 700:
        return IMAGE_ASSETS[4];
      case 800:
        return IMAGE_ASSETS[5];
      case 801:
        return IMAGE_ASSETS[6];
      case 802:
        return IMAGE_ASSETS[7];
      default:
        return IMAGE_ASSETS[8];
    }
  }
}
