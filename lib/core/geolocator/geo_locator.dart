// ignore_for_file: constant_identifier_names

import 'package:clima/core/errors/exceptions.dart';
import 'package:geolocator/geolocator.dart';

const LOCATION_SERVICE_NOT_ENABLED_FAILURE =
    'The location service on the device is disabled.';
const PERMISSION_DENIED =
    'Access to the location of the device is denied by the user.';

abstract class GeoLocatorService {
  Future<Position> getPosition();
}

class GeoLocatorServiceImpl implements GeoLocatorService {
  @override
  Future<Position> getPosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      throw GeoException(LOCATION_SERVICE_NOT_ENABLED_FAILURE);
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        throw GeoException(PERMISSION_DENIED);
      }
    }

    if (permission == LocationPermission.deniedForever) {
      throw GeoException(PERMISSION_DENIED);
    }
    return await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.low);
  }
}
