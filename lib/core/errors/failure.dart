import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  final List properties;

  const Failure({this.properties = const []});

  @override
  List<Object?> get props => properties;
}

class GeoFailure extends Failure {
  final String message;

  const GeoFailure(this.message);

  @override
  List<Object?> get props => [message];
}

class NetworkFailure extends Failure {
  
}
class ServerFailure extends Failure{
  
}

