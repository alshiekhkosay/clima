class GeoException implements Exception {
  final String message;

  GeoException(this.message);
}

class ServerException implements Exception {
  
}