import 'package:clima/core/app_const.dart';
import 'package:clima/features/weather/presentation/Bloc/weather_bloc.dart';
import 'package:clima/features/weather/presentation/Pages/splash/splash_screen.dart';
import 'package:clima/injection_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'injection_container.dart' as di;

void _preChacheImages(context) {
  for (var image in AppConsts.IMAGE_ASSETS) {
    precacheImage(image, context);
  }
}

void main() {
  di.init();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void didChangeDependencies() {
    _preChacheImages(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => sl<WeatherBloc>(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            textTheme: const TextTheme(
          titleLarge: TextStyle(
              fontSize: 20, color: Colors.orange, fontWeight: FontWeight.w900),
        )),
        home: const SplashScreen(),
      ),
    );
  }
}
