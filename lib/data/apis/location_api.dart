// ignore_for_file: constant_identifier_names


import 'package:geolocator/geolocator.dart';


const String PERMISSION_DENIED = 'permission denied';

class LocationAPI {
  Future<Position> determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();

    if (!serviceEnabled) {
      throw const LocationServiceDisabledException();
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error(const PermissionDeniedException(PERMISSION_DENIED));
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // return Future.error(PermissionDeniedForEverException(PERMISSION_DENIED));
    }
    return Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.medium);
  }
}
