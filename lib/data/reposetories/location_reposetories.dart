
import 'package:geolocator/geolocator.dart';

import '../../core/errors/failure.dart';
import '../apis/location_api.dart';
import 'package:dartz/dartz.dart';

abstract class LocationRepository {
  final LocationAPI locationAPI = LocationAPI();

  Future<Either<Failure, Position>> getPosition() ;
  // async {
    // try {
    //   final position = await locationAPI.determinePosition();
    //   return Right(position);
    // } catch (e) {
    //   switch (e.runtimeType) {
    //     case LocationServiceDisabledException:
    //       return Left(LocationServiceFailure(e.toString()));
    //     case PermissionDeniedException:
    //       return Left(PermissionFailure(e.toString()));

    //     default:
    //       return Left(ForEverPermissionFailure(e.toString()));
    //   }
    // }
  // }
}
