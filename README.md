# Clima - Weather Application

  

"Clima" is a Flutter project developed using Test-Driven Development (TDD) and adhering to Clean Architecture principles. This application provides users with weather information based on their current location and allows them to search for weather data by city name.

  

## Features

  

- Get real-time weather information for your current location.

- Search for weather information in any city by specifying its name.

  

## Technologies Used

  

- Flutter: An open-source framework for building natively compiled applications for mobile, web, and desktop from a single codebase.

- Dart: The programming language used for developing Flutter applications.

  

## Project Structure

  

The project follows Clean Architecture principles, separating the application into three main layers:

  

1. Presentation: Handles the user interface and user interactions.

2. Domain: Contains the business logic and use cases.

3. Data: Manages data retrieval and storage.

  

## Getting Started

  

To run this project on your local machine, follow these steps:

  

1. Clone the repository:
```bash
git clone https://gitlab.com/alshiekhkosay/clima.git
```
2. Navigate to the project directory:

```bash
cd clima
```
3. Install the required dependencies using Flutter's package manager:
```bash
flutter pub get
```
## Running the Application

To start the "clima" weather application, use the following command within the project directory:
```bash
flutter run
```
This command will compile the code and launch the application on either an emulator or a connected physical device, depending on your Flutter configuration.

```bash
flutter test
```
This command will execute all the tests to ensure the correctness and reliability of the application's features.

## Contributing

Contributions to this project are welcome! If you have suggestions, bug reports, or would like to contribute code, please open an issue or create a pull request.

## Acknowledgments

Special thanks to the Flutter and Dart communities for their invaluable resources and support