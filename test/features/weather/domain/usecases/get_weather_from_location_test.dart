import 'package:clima/features/weather/domain/entities/weather.dart';
import 'package:clima/features/weather/domain/repositories/weather_repository.dart';
import 'package:clima/features/weather/domain/usecases/get_weather_from_location.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'get_weather_from_location_test.mocks.dart';

@GenerateNiceMocks([MockSpec<WeatherRepository>()])
void main() {
  late GetWeatherFromLocation getWeatherFromLocation;
  late MockWeatherRepository mockWeatherRepository;

  setUp(() {
    mockWeatherRepository = MockWeatherRepository();
    getWeatherFromLocation = GetWeatherFromLocation(mockWeatherRepository);
  });

  test('should return weather for the location provided', () async {
    final weather = Weather(
        id: 802,
        cityName: 'Hama',
        description: 'scattered clouds',
        humidity: 67,
        icon: 'ome',
        sunRise: DateTime.fromMillisecondsSinceEpoch(1696735956 * 1000),
        sunSet: DateTime.fromMillisecondsSinceEpoch(1696777706 * 1000),
        temp: 296,
        timeZone: 10800,
        windSpeed: 2.89);
    //arrenge
    when(mockWeatherRepository.getWeatherFromLocation())
        .thenAnswer((_) async => Right(weather));
    //act
    final result = await getWeatherFromLocation(const NoParams());
    //assert
    verify(mockWeatherRepository.getWeatherFromLocation());
    expect(result, Right(weather));
    verifyNoMoreInteractions(mockWeatherRepository);
  });
}
