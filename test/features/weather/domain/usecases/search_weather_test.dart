import 'package:clima/features/weather/domain/entities/weather.dart';
import 'package:clima/features/weather/domain/usecases/search_weather.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import 'get_weather_from_location_test.mocks.dart';

void main() {
  late MockWeatherRepository mockWeatherRepository;
  late SearchWeather searchWeather;

  setUp(() {
    mockWeatherRepository = MockWeatherRepository();
    searchWeather = SearchWeather(mockWeatherRepository);
  });

  test('should return a Weather for a search with city Name', () async {
    final weather = Weather(
        id: 802,
        cityName: 'Hama',
        description: 'scattered clouds',
        humidity: 67,
        icon: 'ome',
        sunRise: DateTime.fromMillisecondsSinceEpoch(1696735956 * 1000),
        sunSet: DateTime.fromMillisecondsSinceEpoch(1696777706 * 1000),
        temp: 296,
        timeZone: 10800,
        windSpeed: 2.89);
    //arrenge
    when(mockWeatherRepository.searchWeather(any))
        .thenAnswer((_) async => Right(weather));
    //act
    final result = await searchWeather(const SearchParams('Hama'));
    //assert
    verify(mockWeatherRepository.searchWeather('Hama'));
    expect(result, Right(weather));
    verifyNoMoreInteractions(mockWeatherRepository);
  });
}
