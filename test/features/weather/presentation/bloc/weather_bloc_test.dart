import 'package:clima/core/errors/failure.dart';
import 'package:clima/core/geolocator/geo_locator.dart';
import 'package:clima/features/weather/domain/entities/weather.dart';
import 'package:clima/features/weather/domain/usecases/get_weather_from_location.dart';
import 'package:clima/features/weather/domain/usecases/search_weather.dart';
import 'package:clima/features/weather/presentation/Bloc/weather_bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'weather_bloc_test.mocks.dart';

@GenerateNiceMocks([
  MockSpec<GetWeatherFromLocation>(),
  MockSpec<SearchWeather>(),
])
void main() {
  late MockGetWeatherFromLocation mockGetWeatherFromLocation;
  late MockSearchWeather mockSearchWeather;
  late WeatherBloc weatherBloc;

  setUp(() {
    mockGetWeatherFromLocation = MockGetWeatherFromLocation();
    mockSearchWeather = MockSearchWeather();
    weatherBloc = WeatherBloc(
      getWeatherFromLocation: mockGetWeatherFromLocation,
      searchWeather: mockSearchWeather,
    );
  });

  test('initial state should be the bloc intial state of the bloc', () {
    expect(weatherBloc.state, WeatherInitial());
  });
  final weather = Weather(
      id: 802,
      cityName: 'Hama',
      description: 'scattered clouds',
      humidity: 67,
      icon: 'ome',
      sunRise: DateTime.fromMillisecondsSinceEpoch(1696735956 * 1000),
      sunSet: DateTime.fromMillisecondsSinceEpoch(1696777706 * 1000),
      temp: 296,
      timeZone: 10800,
      windSpeed: 2.89);
  group('getWeatherForLocation', () {
    test('should get data from the GetWeatherForLocation use case', () async {
      //arrenge
      when(mockGetWeatherFromLocation(any))
          .thenAnswer((_) async => Right(weather));
      //act
      weatherBloc.add(GetWeatherForLocation());
      await untilCalled(mockGetWeatherFromLocation(any));
      //assert
      verify(mockGetWeatherFromLocation(const NoParams()));
    });

    test(
        'should emit [Loading,Loaded] when the request to get weather data success',
        () {
      //arrenge
      when(mockGetWeatherFromLocation(any))
          .thenAnswer((_) async => Right(weather));
      //assert later
      final expect = [Loading(), Loaded(weather)];
      expectLater(weatherBloc.stream, emitsInOrder(expect));
      //act
      weatherBloc.add(GetWeatherForLocation());
    });

    test('should emit [Loading,Error] with a proper error message', () {
      //arrenge
      when(mockGetWeatherFromLocation(any)).thenAnswer((_) async =>
          const Left(GeoFailure(LOCATION_SERVICE_NOT_ENABLED_FAILURE)));
      //assert later
      final expect = [
        Loading(),
        const Error(LOCATION_SERVICE_NOT_ENABLED_FAILURE)
      ];
      expectLater(weatherBloc.stream, emitsInOrder(expect));
      //act
      weatherBloc.add(GetWeatherForLocation());
    });

    test(
        'should emit [Loading,Error] with a proper error message when [NetworkFilure]',
        () {
      //arrenge
      when(mockGetWeatherFromLocation(any))
          .thenAnswer((_) async => Left(NetworkFailure()));
      //assert later
      final expect = [Loading(), const Error(NETWORK_FAILURE_MESSAGE)];
      expectLater(weatherBloc.stream, emitsInOrder(expect));
      //act
      weatherBloc.add(GetWeatherForLocation());
    });
    test(
        'should emit [Loading,Error] with a proper error message when [SERVERFAILURE]',
        () {
      //arrenge
      when(mockGetWeatherFromLocation(any))
          .thenAnswer((_) async => Left(ServerFailure()));
      //assert later
      final expect = [Loading(), const Error(SERVER_FAILURE)];
      expectLater(weatherBloc.stream, emitsInOrder(expect));
      //act
      weatherBloc.add(GetWeatherForLocation());
    });
  });

  group('SearchWeatherWithCityName', () {
    const cityName = 'Hama';
    test('should get data from the SearchWeather use case', () async {
      //arrenge
      when(mockSearchWeather(any)).thenAnswer((_) async => Right(weather));
      //act
      weatherBloc.add(const SearchWeatherWithCityName(cityName));
      await untilCalled(mockSearchWeather(any));
      //assert
      verify(mockSearchWeather(const SearchParams(cityName)));
    });

    test(
        'should emit [Loading,Loaded] when the request to get weather data success',
        () {
      //arrenge
      when(mockSearchWeather(any)).thenAnswer((_) async => Right(weather));
      //assert later
      final expect = [SearchLoading(), SearchLoaded(weather)];
      expectLater(weatherBloc.stream, emitsInOrder(expect));
      //act
      weatherBloc.add(const SearchWeatherWithCityName(cityName));
    });

    test('should emit [Loading,Error] with a proper error message', () {
      //arrenge
      when(mockSearchWeather(any)).thenAnswer((_) async =>
          const Left(GeoFailure(LOCATION_SERVICE_NOT_ENABLED_FAILURE)));
      //assert later
      final expect = [
        SearchLoading(),
        const SearchError(LOCATION_SERVICE_NOT_ENABLED_FAILURE)
      ];
      expectLater(weatherBloc.stream, emitsInOrder(expect));
      //act
      weatherBloc.add(const SearchWeatherWithCityName(cityName));
    });

    test(
        'should emit [Loading,Error] with a proper error message when [NetworkFilure]',
        () {
      //arrenge
      when(mockSearchWeather(any))
          .thenAnswer((_) async => Left(NetworkFailure()));
      //assert later
      final expect = [
        SearchLoading(),
        const SearchError(NETWORK_FAILURE_MESSAGE)
      ];
      expectLater(weatherBloc.stream, emitsInOrder(expect));
      //act
      weatherBloc.add(const SearchWeatherWithCityName(cityName));
    });
    test(
        'should emit [Loading,Error] with a proper error message when [SERVERFAILURE]',
        () {
      //arrenge
      when(mockSearchWeather(any))
          .thenAnswer((_) async => Left(ServerFailure()));
      //assert later
      final expect = [SearchLoading(), const SearchError(SERVER_FAILURE)];
      expectLater(weatherBloc.stream, emitsInOrder(expect));
      //act
      weatherBloc.add(const SearchWeatherWithCityName(cityName));
    });
  });
}
