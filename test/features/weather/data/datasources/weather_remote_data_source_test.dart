import 'dart:convert';

import 'package:clima/core/errors/exceptions.dart';
import 'package:clima/features/weather/data/datasources/weather_remote_data_source.dart';
import 'package:clima/features/weather/data/models/weather_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';

import '../../../../fixtures/fixture.dart';
import 'weather_remote_data_source_test.mocks.dart';

@GenerateNiceMocks([
  MockSpec<http.Client>(),
])
void main() {
  late MockClient mockClient;
  late WeatherRemoteDataSourceImpl dataSource;

  setUp(() {
    mockClient = MockClient();
    dataSource = WeatherRemoteDataSourceImpl(mockClient);
  });
  const long = 36.7578;
  const lat = 35.1318;
  final weather = WeatherModel.fromJson(jsonDecode(fixture('weather.json')));

  setUpMockClientResponse200() {
    when(mockClient.get(any))
        .thenAnswer((_) async => http.Response(fixture('weather.json'), 200));
  }

  setUpMockClientResponse401() {
    when(mockClient.get(any))
        .thenAnswer((_) async => http.Response('server error', 401));
  }

  group('getWeatherForLocation', () {
    test('should perform Get request on URL with lang and lat query', () async {
      //arrenge
      setUpMockClientResponse200();
      //act
      await dataSource.getWeatherFromLocation(long, lat);
      //assert
      verify(mockClient.get(Uri.parse(
          'https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$long&appid=$APP_ID&units=metric')));
    });
    test('should return a valid weather model when the request is successful',
        () async {
      //arrenge
      setUpMockClientResponse200();
      //act
      final result = await dataSource.getWeatherFromLocation(long, lat);
      //assert
      expect(result, weather);
    });

    test('should throw [ServerException] when the request is not successful',
        () async {
      //arrenge
      setUpMockClientResponse401();
      //act
      final result = dataSource.getWeatherFromLocation;
      //assert
      expect(() => result(long, lat),
          throwsA(const TypeMatcher<ServerException>()));
    });
  });

  group('SearchWeather', () {
    test('should perform Get request on URL with city name', () async {
      //arrenge
      setUpMockClientResponse200();
      //act
      await dataSource.searchWeather('Hama');
      //assert
      verify(mockClient.get(Uri.parse(
          'https://api.openweathermap.org/data/2.5/weather?q=Hama&appid=$APP_ID&units=metric')));
    });
    test('should return a valid weather model when the request is successful',
        () async {
      //arrenge
      setUpMockClientResponse200();
      //act
      final result = await dataSource.searchWeather('Hama');
      //assert
      expect(result, weather);
    });

    test('should throw [ServerException] when the request is not successful',
        () async {
      //arrenge
      setUpMockClientResponse401();
      //act
      final result = dataSource.searchWeather;
      //assert
      expect(() => result('Hama'),
          throwsA(const TypeMatcher<ServerException>()));
    });
  });
}
