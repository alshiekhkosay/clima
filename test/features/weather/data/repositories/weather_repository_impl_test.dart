import 'package:clima/core/errors/exceptions.dart';
import 'package:clima/core/errors/failure.dart';
import 'package:clima/core/geolocator/geo_locator.dart';
import 'package:clima/core/networkinfo/network_info.dart';
import 'package:clima/features/weather/data/datasources/weather_remote_data_source.dart';
import 'package:clima/features/weather/data/models/weather_model.dart';
import 'package:clima/features/weather/data/repositories/weather_repository_impl.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'weather_repository_impl_test.mocks.dart';

@GenerateNiceMocks([
  MockSpec<GeoLocatorService>(),
  MockSpec<WeahterRemoteDataSource>(),
  MockSpec<NetworkInfo>()
])
void main() {
  late WeatherRepositoryImpl repositoryImpl;
  late MockGeoLocatorService mockGeoLocatorService;
  late MockWeahterRemoteDataSource mockWeahterRemoteDataSource;
  late MockNetworkInfo mockNetworkInfo;
  setUp(() {
    mockGeoLocatorService = MockGeoLocatorService();
    mockWeahterRemoteDataSource = MockWeahterRemoteDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repositoryImpl = WeatherRepositoryImpl(
      mockWeahterRemoteDataSource,
      mockGeoLocatorService,
      mockNetworkInfo,
    );
  });

  final weather = WeatherModel(
      id: 802,
      cityName: 'Hama',
      description: 'scattered clouds',
      humidity: 67,
      icon: 'ome',
      sunRise: DateTime.fromMillisecondsSinceEpoch(1696735956 * 1000),
      sunSet: DateTime.fromMillisecondsSinceEpoch(1696777706 * 1000),
      temp: 296,
      timeZone: 10800,
      windSpeed: 2.89);
  final position = Position(
      longitude: 36.7578,
      latitude: 35.1318,
      timestamp: DateTime.now(),
      accuracy: 2,
      altitude: 3,
      heading: 3,
      speed: 5,
      speedAccuracy: 5);

  group('Get Weather from a location', () {
    test('should call  getPosition', () {
      //arrenge
      when(mockGeoLocatorService.getPosition())
          .thenAnswer((_) async => position);
      //act
      repositoryImpl.getWeatherFromLocation();
      //assert
      verify(mockGeoLocatorService.getPosition());
    });

    test('should return Failure if the permission is Denied', () async {
      //arrenge
      when(mockGeoLocatorService.getPosition())
          .thenThrow(GeoException(PERMISSION_DENIED));
      //act
      final result = await repositoryImpl.getWeatherFromLocation();
      //assert
      verify(mockGeoLocatorService.getPosition());
      expect(
        result,
        equals(const Left(GeoFailure(PERMISSION_DENIED))),
      );
    });

    test('should return Failure if the service is disable', () async {
      //arrenge
      when(mockGeoLocatorService.getPosition())
          .thenThrow(GeoException(LOCATION_SERVICE_NOT_ENABLED_FAILURE));
      //act
      final result = await repositoryImpl.getWeatherFromLocation();
      //assert
      verify(mockGeoLocatorService.getPosition());
      expect(
        result,
        equals(const Left(GeoFailure(LOCATION_SERVICE_NOT_ENABLED_FAILURE))),
      );
    });

    test('should call isConnected', () async {
      //arrenge
      when(mockGeoLocatorService.getPosition())
          .thenAnswer((_) async => position);
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      //act
      await repositoryImpl.getWeatherFromLocation();
      //assert
      verify(mockNetworkInfo.isConnected);
    });

    test('should return a weather data when there is a connection', () async {
      //arrenge
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      when(mockGeoLocatorService.getPosition())
          .thenAnswer((_) async => position);
      when(mockWeahterRemoteDataSource.getWeatherFromLocation(any, any))
          .thenAnswer((_) async => weather);
      //act
      final result = await repositoryImpl.getWeatherFromLocation();
      //assert
      // verify(mockNetworkInfo.isConnected);
      verify(mockWeahterRemoteDataSource.getWeatherFromLocation(any, any));
      expect(result, equals(Right(weather)));
    });

    test('should return a network failure when there is no connection',
        () async {
      //arrenge
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      //act
      final result = await repositoryImpl.getWeatherFromLocation();
      //assert
      verify(mockNetworkInfo.isConnected);
      expect(result, equals(Left(NetworkFailure())));
    });

    test('should return a server failure when an error accured from the server',
        () async {
      //arrenge
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      when(mockGeoLocatorService.getPosition())
          .thenAnswer((_) async => position);
      when(mockWeahterRemoteDataSource.getWeatherFromLocation(any, any))
          .thenThrow(ServerException());
      //act
      final result = await repositoryImpl.getWeatherFromLocation();
      //assert
      verify(mockWeahterRemoteDataSource.getWeatherFromLocation(any, any));
      expect(result, equals(Left(ServerFailure())));
    });
  });

  group('search weather', () {
    const tCityName = 'Hama';
    test('should Call Network.isConnected', () async {
      //arrenge
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      //act
      await repositoryImpl.searchWeather(tCityName);

      //assert
      verify(mockNetworkInfo.isConnected);
    });

    test('should return valid weather data when the reqest is sucessful',
        () async {
      //arrenge
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      when(mockWeahterRemoteDataSource.searchWeather(any))
          .thenAnswer((_) async => weather);
      //act
      final result = await repositoryImpl.searchWeather(tCityName);

      //assert
      expect(result, equals(Right(weather)));
    });
    test('should return network failure when there is no connection', () async {
      //arrenge
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);

      //act
      final result = await repositoryImpl.searchWeather(tCityName);

      //assert
      expect(result, equals(Left(NetworkFailure())));
    });

    test('should return a server failure when an error accured from the server',
        () async {
      //arrenge
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      when(mockGeoLocatorService.getPosition())
          .thenAnswer((_) async => position);
      when(mockWeahterRemoteDataSource.searchWeather(any))
          .thenThrow(ServerException());
      //act
      final result = await repositoryImpl.searchWeather('Hama');
      //assert
      verify(mockWeahterRemoteDataSource.searchWeather('Hama'));
      expect(result, equals(Left(ServerFailure())));
    });
  });
}
