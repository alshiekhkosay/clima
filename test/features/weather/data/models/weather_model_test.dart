import 'dart:convert';

import 'package:clima/features/weather/data/models/weather_model.dart';
import 'package:clima/features/weather/domain/entities/weather.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../fixtures/fixture.dart';

void main() {
  final weather = WeatherModel(
      id: 802,
      cityName: 'Hama',
      description: 'scattered clouds',
      humidity: 67,
      icon: 'ome',
      sunRise: DateTime.fromMillisecondsSinceEpoch(1696735956 * 1000),
      sunSet: DateTime.fromMillisecondsSinceEpoch(1696777706 * 1000),
      temp: 296.79,
      timeZone: 10800,
      windSpeed: 2.89);
  test('Weather Model should be extended from weather', () {
    expect(weather, isA<Weather>());
  });

  group('from and to Json', () {
    test('should return a valid weather model', () {
      //arrenge
      final Map<String, dynamic> jsonMap = json.decode(fixture('weather.json'));
      //act
      final result = WeatherModel.fromJson(jsonMap);
      //assert
      expect(result, weather);
    });

    test('should retun a valid map ', () {
      //act
      final jsonMap = weather.toJson();
      final expectedMap = {
        'name': 'Hama',
        'weather': [
          {
            'description': 'scattered clouds',
            'icon': 'ome',
            "id": 802,
          }
        ],
        'main': {'humidity': 67, 'temp': 296.79},
        'sys': {'sunrise': 1696735956, 'sunset': 1696777706},
        'wind': {'speed': 2.89},
        'timezone': 10800
      };
      //assert
      expect(jsonMap, expectedMap);
    });
  });
}
